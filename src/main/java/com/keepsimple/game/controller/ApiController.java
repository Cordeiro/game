package com.keepsimple.game.controller;


import com.keepsimple.game.dao.MatchService;
import com.keepsimple.game.dao.PlayerService;
import com.keepsimple.game.dao.WrangleService;
import com.keepsimple.game.model.Match;
import com.keepsimple.game.model.Player;
import com.keepsimple.game.model.Wrangle;
import com.keepsimple.game.model.json.RankingMatch;
import com.keepsimple.game.model.json.RankingPlayer;
import com.keepsimple.game.model.json.RankingWeapon;
import com.keepsimple.game.util.ResponseJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalTime;
import java.util.*;

@RestController
@RequestMapping("api/v1")
public class ApiController {
    //private static final Logger logger = LogManager.getLogger(ApiController.class);

    @Autowired private MatchService matchService;
    @Autowired private PlayerService playerService;
    @Autowired private WrangleService wrangleService;

    /*USADO P/ POPULAR OS JSON RECEBIDOS*/
    @RequestMapping(value = "match/all" , method = RequestMethod.POST)
    public ResponseEntity readAllMatch (@RequestBody List<Match> matches){
        matches.forEach(match ->{
                    match.getPlayers().forEach(player ->
                        player.setMatches(Arrays.asList(match))
                    );
            playerService.save(match.getPlayers());
            matchService.save(match);
            System.out.println(match);
        });
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /*USADO P/ POPULAR OS JSON RECEBIDOS*/
    @RequestMapping(value = "wrangle/all" , method = RequestMethod.POST)
    public ResponseEntity readAllWrangle (@RequestBody List<Wrangle> wrangles){
        ResponseJson<Wrangle> responseJson = new ResponseJson<>();
        wrangles.forEach(wrangle -> {
            if(Optional.ofNullable(matchService.findOne(wrangle.getMatch())).isPresent()){
                System.out.println("[readWrangle] SALVO " +wrangle);
                wrangleService.save(wrangle);
            }else {
                responseJson.getErros().add("Wrangle com match '"+wrangle.getMatch()+"' não encontrado");
            }
        });
        if (responseJson.getErros().isEmpty()){
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }else {
            return ResponseEntity.ok(responseJson);
        }
    }

    @RequestMapping(value = "wrangle/all" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Wrangle> readAllWrangle(){
        return wrangleService.findAll();
    }

    @RequestMapping(value = "match/all" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Match> readAllMatch(){
        return matchService.findAll();
    }

    @RequestMapping(value = "ranking/player" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RankingPlayer> rankPlayer(){
        List<Player> players = playerService.findAll();
        List<RankingPlayer> rankingPlayers = new ArrayList<>();
        Set<String> playersDistinct = new HashSet<>();
        players.forEach(player -> playersDistinct.addAll(player.getNames()));

        playersDistinct.forEach(name->{
            Long killer = wrangleService.findPlayerCountKiller(name);
            Long killed = wrangleService.findPlayerCountKilled(name);
            System.out.println("Name: '"+name+"'\tCount Killer: " +killer+ "\tCount Killed: " +killed+" \tPontuacao: " + (killer - killed));
            rankingPlayers.add(new RankingPlayer(name,killer,killed));
        });
        rankingPlayers.sort(Comparator.comparingLong(RankingPlayer::getPontuacao).reversed());
        return rankingPlayers;
    }

    @RequestMapping(value = "ranking/player/{begin}/{end}" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RankingPlayer> rankPlayerWithDate(@PathVariable("begin") @DateTimeFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssXXX") Date begin,
                                          @PathVariable("end") @DateTimeFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssXXX") Date end){
        System.out.println("Data inicio: " +begin+ "\tData fim: "+end);
        List<Player> players = playerService.findAll();
        List<RankingPlayer> rankingPlayers = new ArrayList<>();
        Set<String> playersDistinct = new HashSet<>();
        players.forEach(player -> playersDistinct.addAll(player.getNames()));

        playersDistinct.forEach(name->{
            Long killer = wrangleService.findPlayerCountKillerWithDate(name,begin, end);
            Long killed = wrangleService.findPlayerCountKilledWithDate(name,begin, end);
            System.out.println("Name: '"+name+"'\tCount Killer: " +killer+ "\tCount Killed: " +killed+" \tPontuacao: " + (killer - killed));
            rankingPlayers.add(new RankingPlayer(name,killer,killed));
        });
        rankingPlayers.sort(Comparator.comparingLong(RankingPlayer::getPontuacao).reversed());
        return rankingPlayers;
    }

    @RequestMapping(value = "ranking/weapon" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RankingWeapon> rankWeapon(){
        List<RankingWeapon> rankingWeapons = wrangleService.findRankingWeapon();
        rankingWeapons.sort(Comparator.comparingLong(RankingWeapon::getKiller).reversed());
        rankingWeapons.forEach(System.out::println);
        return rankingWeapons;
    }

    @RequestMapping(value = "ranking/weapon/{begin}/{end}" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RankingWeapon> rankWeaponWithDate(@PathVariable("begin") @DateTimeFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssXXX") Date begin,
                                          @PathVariable("end") @DateTimeFormat(pattern= "yyyy-MM-dd'T'HH:mm:ssXXX") Date end){
        List<Long> matches = matchService.findMatchBetweenDates(begin, end);
        List<RankingWeapon> rankingWeapons = wrangleService.findRankingWeaponWithMatch(matches);
        rankingWeapons.sort(Comparator.comparingLong(RankingWeapon::getKiller).reversed());
        rankingWeapons.forEach(System.out::println);
        return rankingWeapons;
    }

    @RequestMapping(value = "wrangle" , method = RequestMethod.POST)
    public ResponseEntity<ResponseJson<Wrangle>> newMatch (@RequestBody @Valid Wrangle wrangle, BindingResult result){
        ResponseJson<Wrangle> responseEntity = new ResponseJson<>();
        if(result.hasErrors()){
            result.getAllErrors().forEach(error -> responseEntity.getErros().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(responseEntity);
        }
        if(Optional.ofNullable(matchService.findOne(wrangle.getMatch())).isPresent()){
            wrangleService.save(wrangle);
            System.out.println("Salvo "+wrangle);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }else {
            responseEntity.getErros().add("Wrangle com match '"+wrangle.getMatch()+"' não encontrado");
            return ResponseEntity.badRequest().body(responseEntity);
        }
    }

    @RequestMapping(value = "ranking/match" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RankingMatch> rankMatches(){
        List<RankingMatch> rankingMatchList = new ArrayList<>();
        List<Match> matches = matchService.findAll();

        matches.forEach(match -> {
            RankingMatch rankingMatch = new RankingMatch();
            mapRankingMatch(match, rankingMatch);
            rankingMatchList.add(rankingMatch);
        });
        rankingMatchList.sort(Comparator.comparingLong(value -> value.getMatch().getId()));
        return rankingMatchList;
    }

    @RequestMapping(value = "ranking/match/{id}" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RankingPlayer> rankMatchWithDate(@PathVariable("id") Long id){
        System.out.println("rankMatchWithDate: "+ id);
        List<RankingPlayer> rankingPlayers = new ArrayList<>();
        List<Player> players = playerService.findAll();
        Set<String> playersDistinct = new HashSet<>();
        players.forEach(player -> playersDistinct.addAll(player.getNames()));

        playersDistinct.forEach(name->{
            Long killer = wrangleService.findPlayerCountKillerWithMatch(name,id);
            Long killed = wrangleService.findPlayerCountKilledWithMatch(name,id);
            System.out.println("Name: '"+name+"'\tCount Killer: " +killer+ "\tCount Killed: " +killed+" \tPontuacao: " + (killer - killed));
            rankingPlayers.add(new RankingPlayer(name,killer,killed));
        });
        rankingPlayers.sort(Comparator.comparingLong(RankingPlayer::getPontuacao).reversed());
        return rankingPlayers;
    }

    private void mapRankingMatch(Match match, RankingMatch rankingMatch) {
        rankingMatch.setMatch(match);
        rankingMatch.setPunctuation(0L);
        System.out.println("\n"+match);
        match.getPlayers().forEach(player -> player.getNames().forEach(name->{
            Long killer = wrangleService.findPlayerCountKillerWithMatch(name,match.getId());
            Long killed = wrangleService.findPlayerCountKilledWithMatch(name,match.getId());
            Long aux = killer - killed;
            rankingMatch.setPlayers(player.getNames().size());

            System.out.println("Name: '"+name+"'\tCount Killer: " +killer+ "\tCount Killed: " +killed+" \tPontuacao: " + aux);
            if (aux > rankingMatch.getPunctuation()){
                System.out.println("Old Champion: "+rankingMatch.getChampion()+"\tPoint: "+rankingMatch.getPunctuation());
                rankingMatch.setPunctuation(aux);
                rankingMatch.setChampion(name);
                System.out.println("New Champion: "+rankingMatch.getChampion()+"\tPoint: "+rankingMatch.getPunctuation());
            }
        }));
    }

}