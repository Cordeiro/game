package com.keepsimple.game.dao;

import com.keepsimple.game.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface MatchService extends JpaRepository<Match,Long>{

    @Query("SELECT m.id FROM Match m WHERE m.inicio >= ?1 AND m.fim <= ?2")
    List<Long> findMatchBetweenDates(Date inicio, Date fim);
}