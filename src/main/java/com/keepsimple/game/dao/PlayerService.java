package com.keepsimple.game.dao;

import com.keepsimple.game.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerService extends JpaRepository<Player, Long> {


}