package com.keepsimple.game.dao;

import com.keepsimple.game.model.Player;
import com.keepsimple.game.model.Wrangle;
import com.keepsimple.game.model.json.RankingWeapon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface WrangleService extends JpaRepository<Wrangle, Long>{

    @Query("SELECT COUNT (w) FROM Wrangle w WHERE w.killer in (?1)")
    Long findPlayerCountKiller(String name);

    @Query("SELECT COUNT (w) FROM Wrangle w WHERE w.killed in (?1)")
    Long findPlayerCountKilled(String name);

    @Query("SELECT COUNT (w) FROM Wrangle w WHERE w.killer in (?1) AND w.match = ?2")
    Long findPlayerCountKillerWithMatch(String name, Long match);

    @Query("SELECT COUNT (w) FROM Wrangle w WHERE w.killed in (?1) AND w.match = ?2")
    Long findPlayerCountKilledWithMatch(String name, Long match);

    @Query(value = "SELECT COUNT (w) FROM wrangle w INNER JOIN match m ON w.match = m.id " +
            "WHERE w.killer in (?1) AND m.inicio >= (?2) AND m.fim <= (?3)", nativeQuery = true)
    Long findPlayerCountKillerWithDate(String name, Date begin, Date end);

    @Query(value = "SELECT COUNT (w) FROM wrangle w INNER JOIN match m ON w.match = m.id " +
            "WHERE w.killed in (?1) AND m.inicio >= (?2) AND m.fim <= (?3)", nativeQuery = true)
    Long findPlayerCountKilledWithDate(String name, Date begin, Date end);

    @Query("SELECT new com.keepsimple.game.model.json.RankingWeapon(w.weapon, COUNT(w)) FROM Wrangle w GROUP BY w.weapon")
    List<RankingWeapon> findRankingWeapon();

    @Query("SELECT new com.keepsimple.game.model.json.RankingWeapon(w.weapon, COUNT(w)) FROM Wrangle w WHERE w.match in (?1) " +
            "GROUP BY w.weapon")
    List<RankingWeapon> findRankingWeaponWithMatch(List<Long> id);

}