package com.keepsimple.game.model.json;

public class RankingWeapon {

    private String weapon;
    private Long killer;

    public RankingWeapon(String weapon, Long killer) {
        this.weapon = weapon;
        this.killer = killer;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public Long getKiller() {
        return killer;
    }

    public void setKiller(Long killer) {
        this.killer = killer;
    }

    @Override
    public String toString() {
        return "RankingWeapon{" +
                "weapon='" + weapon + '\'' +
                ", killer=" + killer +
                '}';
    }
}