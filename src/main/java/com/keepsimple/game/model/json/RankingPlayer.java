package com.keepsimple.game.model.json;

public class RankingPlayer {

    private String nome;
    private Long killer;
    private Long killed;
    private Long pontuacao;

    public RankingPlayer(String nome, Long killer, Long killed) {
        this.nome = nome;
        this.killer = killer;
        this.killed = killed;
        this.pontuacao = killer - killed;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getKiller() {
        return killer;
    }

    public void setKiller(Long killer) {
        this.killer = killer;
    }

    public Long getKilled() {
        return killed;
    }

    public void setKilled(Long killed) {
        this.killed = killed;
    }

    public Long getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(Long pontuacao) {
        this.pontuacao = pontuacao;
    }

    @Override
    public String toString() {
        return "RankingPlayer{" +
                "nome='" + nome + '\'' +
                ", killer=" + killer +
                ", killed=" + killed +
                ", pontuacao=" + pontuacao +
                '}';
    }
}
