package com.keepsimple.game.model.json;

import com.keepsimple.game.model.Match;

import java.util.List;

public class RankingMatch {

    private Match match;
    private int players;
    private String champion;
    private Long punctuation;

    public RankingMatch(){  }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public int getPlayers() {
        return players;
    }

    public void setPlayers(int players) {
        this.players = players;
    }

    public String getChampion() {
        return champion;
    }

    public void setChampion(String champion) {
        this.champion = champion;
    }

    public Long getPunctuation() {
        return punctuation;
    }

    public void setPunctuation(Long punctuation) {
        this.punctuation = punctuation;
    }

    @Override
    public String toString() {
        return "RankingMatch{" +
                "match=" + match +
                ", players=" + players +
                ", champion='" + champion + '\'' +
                ", punctuation=" + punctuation +
                '}';
    }
}
