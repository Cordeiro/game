package com.keepsimple.game.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Match{

    @Id
    @JsonProperty("match")
    private Long id;
    @JsonProperty("begin")
    private Date inicio;
    @JsonProperty("end")
    private Date fim;
    @ManyToMany
    @JoinTable(name = "match_player")
    private List<Player> players;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    @Override
    public String toString() {
        return "Match{" +
                "match=" + id +
                ",begin=" + inicio +
                ", end=" + fim +
                ", players=" + players+
                '}';
    }
}