package com.keepsimple.game.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Entity
public class Wrangle extends AbstractEntity{
    @NotNull(message = "Match não permite nulo")
    private Long match;
    @NotNull(message = "Killtime não permite nulo")
    private LocalTime killtime;
    @NotEmpty(message = "Killer não permite nulo")
    private String killer;
    @NotEmpty(message = "Killed não permite nulo")
    private String killed;
    @NotEmpty(message = "Weapon não permite nulo")
    private String weapon;

    public Long getMatch() {
        return match;
    }

    public void setMatch(Long match) {
        this.match = match;
    }

    @JsonGetter("killtime")
    public String getKillTimeToJson() {
        return killtime.toString();
    }

    public LocalTime getKilltime() {
        return killtime;
    }

    public void setKilltime(String killtime) {
        this.killtime = LocalTime.parse(killtime);
    }

    public String getKiller() {
        return killer;
    }

    public void setKiller(String killer) {
        this.killer = killer;
    }

    public String getKilled() {
        return killed;
    }

    public void setKilled(String killed) {
        this.killed = killed;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    @Override
    public String toString() {
        return "Wrangle{" +
                "id='" + getId() + '\'' +
                ", match='" + match + '\'' +
                ", killtime=" + killtime.toString() +
                ", killer='" + killer + '\'' +
                ", killed='" + killed + '\'' +
                ", weapon='" + weapon + '\'' +
                '}';
    }
}