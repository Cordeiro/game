package com.keepsimple.game.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Player extends AbstractEntity{

    @ManyToMany(mappedBy = "players")
    @JsonIgnore
    private List<Match> matches;
    @Transient
    @JsonIgnore
    private String nameJson;
    @JsonIgnore
    @ElementCollection
    private List<String> names;

    public Player(){
        this.names = new ArrayList<>();
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    @JsonIgnore
    public String getNameJson() {
        return nameJson;
    }
    @JsonProperty("name")
    public void setNameJson(String nameJson) {
        this.names.add(nameJson);
    }

    @JsonProperty
    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id='" + getId() + '\'' +
                ", nameJson='" + nameJson + '\'' +
                ", names=" + names +
                '}';
    }
}